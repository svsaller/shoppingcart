## **Shopping Cart Demonstration - Pragramming Assessment**

### **How to Build and Run** ###

* Clone repository or download the zip: 

```
#!sh

git clone https://svsaller@bitbucket.org/svsaller/shoppingcart.git
```

* Build with maven:

```
#!sh

mvn clean install
```

* Deploy and Run with docker-compose:

```
#!sh

docker-compose up
```
* Or deploy manually to Wildfly (for Java EE 7).
* Access the page at:

```
#!sh

http://localhost:8080
```
### Dependencies ###

* [Docker & docker-compose](https://www.docker.com/) OR [Wildfly 10](http://wildfly.org/)