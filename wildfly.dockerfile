FROM jboss/wildfly
RUN /opt/jboss/wildfly/bin/add-user.sh admin admin
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
COPY target/ShoppingCart-1.0-SNAPSHOT.war /opt/jboss/wildfly/standalone/deployments/ROOT.war


