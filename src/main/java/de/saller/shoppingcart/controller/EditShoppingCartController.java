package de.saller.shoppingcart.controller;

import de.saller.shoppingcart.data.ShoppingCartItemSupplier;
import de.saller.shoppingcart.model.Product;
import de.saller.shoppingcart.model.ShoppingCartItem;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@ViewScoped
@Named
public class EditShoppingCartController implements Serializable {

    @Inject
    private ShoppingCartItemSupplier shoppingCartItemsSupplier;

    private Product selectedProduct;

    public void doRemoveShoppingCartItem(ShoppingCartItem item) {
        shoppingCartItemsSupplier.removeShoppingCartItem(item);
    }

    public String doOK() {
        return PageNavigation.LIST_SHOPPINGCARTS;
    }

    public void updateItemQuantity(ShoppingCartItem item) {
        shoppingCartItemsSupplier.updateShoppingCartItem(item);
    }

    public void addItemToCart() {
        shoppingCartItemsSupplier.addShoppingCartItem(selectedProduct);
    }

    public Product getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }
    
    
}
