package de.saller.shoppingcart.controller;

import de.saller.shoppingcart.data.ShoppingCartItemSupplier;
import de.saller.shoppingcart.model.ShoppingCart;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@ViewScoped
@Named
public class ListShoppingCartsController implements Serializable {
    
    @Inject
    private ShoppingCartItemSupplier shoppingCartItemsSupplier;
    
    public String doEditShoppingCart(ShoppingCart cart) {    
        shoppingCartItemsSupplier.setCurrentShoppingCart(cart);
        return PageNavigation.EDIT_SHOPPINGCART;
    }
    
}
