package de.saller.shoppingcart.controller;

import de.saller.shoppingcart.model.Product;
import de.saller.shoppingcart.services.ProductService;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class ProductConverter implements Converter, Serializable {

    @Inject
    private ProductService productService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
                Long productId = Long.parseLong(value);
                return productService.getProductsById(productId);
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid product", "Please choose a valid product from list."));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof Product) {
            return String.valueOf(((Product) value).getId());
        } else {
            return null;
        }
    }

}
