package de.saller.shoppingcart.controller;

class PageNavigation {

    public static final String LIST_SHOPPINGCARTS = "listShoppingCarts";
    public static final String EDIT_SHOPPINGCART = "editShoppingCart";

}
