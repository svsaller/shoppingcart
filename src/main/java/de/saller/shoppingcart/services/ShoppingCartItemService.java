package de.saller.shoppingcart.services;

import de.saller.shoppingcart.model.ShoppingCart;
import de.saller.shoppingcart.model.ShoppingCartItem;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ShoppingCartItemService {

    @PersistenceContext
    private EntityManager em;

    public void removeShoppingCartItem(ShoppingCartItem cartItem) {
        ShoppingCart managedCart = em.find(ShoppingCart.class, cartItem.getShoppingCart().getId());
        ShoppingCartItem itemEntityToRemove = em.find(ShoppingCartItem.class, cartItem.getId());
        em.remove(itemEntityToRemove);
        em.refresh(managedCart);
    }

    public void updateShoppingCartItem(ShoppingCartItem cartItem) {
        em.merge(cartItem);
    }

    public void addShoppingCartItem(ShoppingCartItem newItem) {
        ShoppingCart managedCart = em.find(ShoppingCart.class, newItem.getShoppingCart().getId());
        managedCart.addShoppingCartItem(newItem);
        em.merge(managedCart);
        em.flush();
    }

}
