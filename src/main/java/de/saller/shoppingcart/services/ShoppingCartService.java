package de.saller.shoppingcart.services;

import de.saller.shoppingcart.model.ShoppingCart;
import de.saller.shoppingcart.model.ShoppingCartItem;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class ShoppingCartService {

    @PersistenceContext
    private EntityManager em;

    public List<ShoppingCart> getAllShoppingCarts() {
        TypedQuery<ShoppingCart> query = em.createNamedQuery(ShoppingCart.findAll, ShoppingCart.class);
        List<ShoppingCart> shoppingcarts = query.getResultList();
        shoppingcarts.forEach(cart -> cart.setTotal(calculateCartTotal(cart)));
        return shoppingcarts;
    }

    public ShoppingCart getShoppingCartById(Long shoppingCartId) {
        ShoppingCart managedCart = em.find(ShoppingCart.class, shoppingCartId);
        managedCart.setTotal(calculateCartTotal(managedCart));
        return managedCart;
    }
    
    double calculateCartTotal(ShoppingCart cart) {
        return cart.getItems().stream()
                .mapToDouble(ShoppingCartItem::getTotal)
                .sum();
    }

}
