package de.saller.shoppingcart.services;

import de.saller.shoppingcart.model.Product;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class ProductService {
    
    @PersistenceContext
    private EntityManager em;
    
    public List<Product> getAllProducts() {
        TypedQuery<Product> query = em.createNamedQuery(Product.findAll, Product.class);
        return query.getResultList();
    }

    public Product getProductsById(Long productId) {
        return em.find(Product.class, productId);
    }
    
}
