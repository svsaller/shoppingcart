package de.saller.shoppingcart.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@NamedQueries({
    @NamedQuery(name = ShoppingCart.findAll, query = "SELECT c FROM ShoppingCart c")
})

@Entity
public class ShoppingCart {

    public static final String findAll = "ShoppingCart.findAll";

    @GeneratedValue
    @Id
    private Long id;
    @OneToMany(mappedBy = "shoppingCart", cascade = CascadeType.MERGE)
    private List<ShoppingCartItem> items;
    @Transient
    private double total;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ShoppingCartItem> getItems() {
        return items;
    }

    public void setItems(List<ShoppingCartItem> items) {
        this.items = items;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getNumberOfItems() {
        return items.size();
    }

    public void addShoppingCartItem(ShoppingCartItem item) {
        this.items.add(item);
    }

}
