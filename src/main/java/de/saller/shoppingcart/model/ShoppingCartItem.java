package de.saller.shoppingcart.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ShoppingCartItem {

    @GeneratedValue
    @Id
    private Long id;
    @ManyToOne
    private Product product;
    @ManyToOne
    private ShoppingCart shoppingCart;
    private int quantity;

    public ShoppingCartItem() {
    }
    
    public ShoppingCartItem(ShoppingCart shoppingCart, Product product) {
        this.shoppingCart = shoppingCart;
        this.product = product;
        this.quantity = 1;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }
            
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public void increaseQuantityByONE() {
        this.quantity++;
    }
    
    public double getTotal() {
        return product.getPrice() * this.quantity;
    }

}
