package de.saller.shoppingcart.data;

import de.saller.shoppingcart.model.ShoppingCart;
import de.saller.shoppingcart.services.ShoppingCartService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

@RequestScoped
public class ShoppingCartSupplier implements Serializable {
    
    private List<ShoppingCart> shoppingCarts;
    
    @Inject
    private ShoppingCartService shoppingCartService;
      
    
    @PostConstruct
    public void init() {
        shoppingCarts = shoppingCartService.getAllShoppingCarts();
    }

    @Produces
    @Named
    public List<ShoppingCart> getShoppingCarts() {
        return shoppingCarts;
    }
    
}
