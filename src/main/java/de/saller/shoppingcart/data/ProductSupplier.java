package de.saller.shoppingcart.data;

import de.saller.shoppingcart.model.Product;
import de.saller.shoppingcart.services.ProductService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

@SessionScoped
public class ProductSupplier implements Serializable{
   
    private List<Product> productList;

    @Inject
    private ProductService productService;

    @Produces
    @Named
    public List<Product> getProductList() {
        return productList;
    }

    @PostConstruct
    public void init() {
        this.productList = productService.getAllProducts();
    }

}
