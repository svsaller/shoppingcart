package de.saller.shoppingcart.data;

import de.saller.shoppingcart.model.Product;
import de.saller.shoppingcart.model.ShoppingCart;
import de.saller.shoppingcart.model.ShoppingCartItem;
import de.saller.shoppingcart.services.ShoppingCartItemService;
import de.saller.shoppingcart.services.ShoppingCartService;
import java.io.Serializable;
import java.util.Optional;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

@SessionScoped
public class ShoppingCartItemSupplier implements Serializable {

    private ShoppingCart currentShoppingCart;

    @Inject
    private ShoppingCartItemService shoppingCartItemService;

    @Inject
    private ShoppingCartService shoppingCartService;

    @Produces
    @Named
    public ShoppingCart getCurrentShoppingCart() {
        return currentShoppingCart;
    }

    public void setCurrentShoppingCart(ShoppingCart currentShoppingCart) {
        this.currentShoppingCart = currentShoppingCart;
    }

    public void updateShoppingCartItem(ShoppingCartItem item) {
        shoppingCartItemService.updateShoppingCartItem(item);
        refreshCurrentShoppingCart();
    }

    public void removeShoppingCartItem(ShoppingCartItem item) {
        shoppingCartItemService.removeShoppingCartItem(item);
        refreshCurrentShoppingCart();
    }

    public void addShoppingCartItem(Product selectedProduct) {
        Optional<ShoppingCartItem> matchingItem = findShoppingCartItemByProduct(selectedProduct);

        if (matchingItem.isPresent()) {
            matchingItem.get().increaseQuantityByONE();
            shoppingCartItemService.updateShoppingCartItem(matchingItem.get());
        } else {
            ShoppingCartItem newItem = new ShoppingCartItem(currentShoppingCart, selectedProduct);
            shoppingCartItemService.addShoppingCartItem(newItem);
        }
        
        refreshCurrentShoppingCart();
    }

    private Optional<ShoppingCartItem> findShoppingCartItemByProduct(Product product) {
        return currentShoppingCart.getItems().stream()
                .filter(item -> item.getProduct().equals(product))
                .findFirst();
    }

    private void refreshCurrentShoppingCart() {
        currentShoppingCart = shoppingCartService.getShoppingCartById(currentShoppingCart.getId());
    }

}
