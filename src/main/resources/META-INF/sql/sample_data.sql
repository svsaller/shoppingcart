insert into PRODUCT (ID, NAME, PRICE) values (10, 'LCD-TV', 1735.9)
insert into PRODUCT (ID, NAME, PRICE) values (20, 'Laser-Printer', 165.5)
insert into PRODUCT (ID, NAME, PRICE) values (30, 'Phone-Cover', 15.95)
insert into PRODUCT (ID, NAME, PRICE) values (40, 'E-Bike', 2246.3)
insert into PRODUCT (ID, NAME, PRICE) values (50, 'Notebook', 967.1)
insert into PRODUCT (ID, NAME, PRICE) values (60, 'LED 15W E27', 6.99)
insert into PRODUCT (ID, NAME, PRICE) values (70, 'USB-Cable', 2.95)
insert into PRODUCT (ID, NAME, PRICE) values (80, 'Battery-Pack 4x 1.5V', 19.4)
insert into PRODUCT (ID, NAME, PRICE) values (90, 'DSL-Router', 169.0)
insert into PRODUCT (ID, NAME, PRICE) values (100, 'SD-CARD 32GB', 15.99)

insert into SHOPPINGCART (ID) values (1000), (2000), (3000)

insert into SHOPPINGCARTITEM (ID, QUANTITY, PRODUCT_ID, SHOPPINGCART_ID) values (10000, 2, 100, 1000), (20000, 3,  60, 1000), (30000, 1,  10, 1000), (40000, 1,  90, 2000), (50000, 5,  60, 2000)
