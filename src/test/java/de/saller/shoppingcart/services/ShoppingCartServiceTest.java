package de.saller.shoppingcart.services;

import de.saller.shoppingcart.model.Product;
import de.saller.shoppingcart.model.ShoppingCart;
import static de.saller.shoppingcart.services.ProductBuilder.aProduct;
import static de.saller.shoppingcart.services.ShoppingCartBuilder.aShoppingCart;
import static de.saller.shoppingcart.services.ShoppingCartItemBuilder.aCartItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.IsCloseTo.closeTo;
import org.junit.Test;

public class ShoppingCartServiceTest {
    
    public ShoppingCart buildTestCart() {
        Product p1 = aProduct().withPrice(0.01).build();
        Product p2 = aProduct().withPrice(1_000_000.99).build();
        Product p3 = aProduct().withPrice(99.99).build();
        Product p4 = aProduct().withPrice(0.0).build();
        
        return aShoppingCart()
                .withCartItem(aCartItem().withProduct(p1).withQuantity(49).build())
                .withCartItem(aCartItem().withProduct(p2).withQuantity(1).build())
                .withCartItem(aCartItem().withProduct(p3).withQuantity(1).build())
                .withCartItem(aCartItem().withProduct(p4).withQuantity(3).build())
                .build();
    }
    
    @Test
    public void testCalculateCartTotal() {
        ShoppingCart testCart = buildTestCart();
        ShoppingCartService cartService = new ShoppingCartService();
        double expected_Total = 1_000_101.47;
        
        double actual_Total = cartService.calculateCartTotal(testCart);

        assertThat(actual_Total, is(closeTo(expected_Total, 0.005)));
    }
    
    
}
