package de.saller.shoppingcart.services;

import de.saller.shoppingcart.model.ShoppingCart;
import de.saller.shoppingcart.model.ShoppingCartItem;
import java.util.ArrayList;
import java.util.Random;

public class ShoppingCartBuilder {

    private ShoppingCart shoppingCart = new ShoppingCart();

    private ShoppingCartBuilder() {
        shoppingCart.setId(new Random().nextLong());
        shoppingCart.setItems(new ArrayList<>());
    }

    public ShoppingCartBuilder withCartItem(ShoppingCartItem item) {
        shoppingCart.addShoppingCartItem(item);
        return this;
    }

    public ShoppingCartBuilder withId(Long id) {
        shoppingCart.setId(id);
        return this;
    }

    public ShoppingCart build() {
        return shoppingCart;
    }

    public static ShoppingCartBuilder aShoppingCart() {
        return new ShoppingCartBuilder();
    }

}
