package de.saller.shoppingcart.services;

import de.saller.shoppingcart.model.Product;
import java.util.Random;

public class ProductBuilder {
    
    private Long id;
    private String name = "SomeProduct";
    private double price = 1.0d;
    
    private ProductBuilder() {
        this.id = new Random().nextLong();
    }
    
    public ProductBuilder withId(Long id) {
        this.id = id;
        return this;
    }
    
    public ProductBuilder withName(String name) {
        this.name = name;
        return this;
    }
    
    public ProductBuilder withPrice(double price) {
        this.price = price;
        return this;
    }
            
    public Product build() {
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setPrice(price);
        return product;
    }
    
    public static ProductBuilder aProduct() {
        return new ProductBuilder();
    }
    
}
