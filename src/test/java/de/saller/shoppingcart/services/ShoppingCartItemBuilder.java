package de.saller.shoppingcart.services;

import de.saller.shoppingcart.model.Product;
import de.saller.shoppingcart.model.ShoppingCart;
import de.saller.shoppingcart.model.ShoppingCartItem;
import java.util.Random;

public class ShoppingCartItemBuilder {

    private Long id;
    private Product product;
    private ShoppingCart shoppingcart;
    private int quantity = 1;
    
    private ShoppingCartItemBuilder() {
        this.id = new Random().nextLong();
    }
    
    public ShoppingCartItemBuilder withId(Long id) {
        this.id = id;
        return this;
    }
    
    public ShoppingCartItemBuilder withProduct(Product product) {
        this.product = product;
        return this;
    }
    
    public ShoppingCartItemBuilder withShoppingCart(ShoppingCart shoppingcart) {
        this.shoppingcart = shoppingcart;
        return this;
    }

    public ShoppingCartItemBuilder withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }
    
    public ShoppingCartItem build() {
        ShoppingCartItem item = new ShoppingCartItem(shoppingcart, product);
        item.setId(id);
        item.setQuantity(quantity);
        return item;
    }
    
    public static ShoppingCartItemBuilder aCartItem() {
        return new ShoppingCartItemBuilder();
    }
    
}
